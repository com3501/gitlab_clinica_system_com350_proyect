<?php
include('conexion.php');

$sql = "SELECT DISTINCT especialidad FROM medico WHERE especialidad <> 'Medico General' AND especialidad <> ' '"; 

$resultado = $con->query($sql);

if ($resultado) {
    $especialidades = array();
    while ($fila = $resultado->fetch_assoc()) {
        $especialidades[] = $fila['especialidad'];
    }


    echo json_encode($especialidades, JSON_UNESCAPED_UNICODE);
} else {

    echo json_encode(['error' => 'Error en la consulta SQL']);
}
?>
