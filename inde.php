<!DOCTYPE html>

<!--
 // WEBSITE: https://themefisher.com
 // TWITTER: https://twitter.com/themefisher
 // FACEBOOK: https://www.facebook.com/themefisher
 // GITHUB: https://github.com/themefisher/
-->

<html lang="en">
<head>

  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <title>Novena- Health Care &amp; Medical template</title>

  <!-- Mobile Specific Metas
  ================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Health Care Medical Html5 Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="author" content="Themefisher">
  <meta name="generator" content="Themefisher Novena HTML Template v1.0">

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png" />

  <!-- 
  Essential stylesheets
  =====================================-->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/icofont/icofont.min.css">
  <link rel="stylesheet" href="plugins/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="plugins/slick-carousel/slick/slick-theme.css">

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="css/style.css">

  <script src="js/fetch.js"></script>
</head>

<body id="top">

<?php
// include('conexion.php');
session_start();

?>
<header>
	<div class="header-top-bar">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<ul class="top-bar-info list-inline-item pl-0 mb-0">
						<li class="list-inline-item"><a href="mailto:support@gmail.com"><i class="icofont-support-faq mr-2"></i>soporte@novena.com</a></li>
						<li class="list-inline-item"><i class="icofont-location-pin mr-2"></i>La dirección Ta-134/A, Nueva York, Estados Unidos.</li>
					</ul>
				</div>
				<div class="col-lg-6">
					<div class="text-lg-right top-right-bar mt-2 mt-lg-0">
						<a href="tel:+23-345-67890">
							<span>Llama Ahora : </span>
							<span class="h4">823-4565-13456</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navigation" id="navbar">
		<div class="container">
			<a class="navbar-brand" href="index.html">
				<img src="images/logo.png" alt="" class="img-fluid">
			</a>

			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain"
				aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
				<span class="icofont-navigation-menu"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarmain">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link" href="inde.php">Inicio</a></li>
					<!-- BUTTON as Paciente -->
					<?php
					if (isset($_SESSION['nivel']) && $_SESSION['nivel'] == 'paciente' ) {?>
							<li class="nav-item"><a class="nav-link" href="javascript:cargarSacarFicha()">Ficha</a></li>
							<!-- <li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('Create_ficha_interface.php')">Ficha</a></li> -->
						<?php   
					}?>
					<?php
					if (isset($_SESSION['nivel']) && $_SESSION['nivel'] == 'paciente' ) {?>
							<li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('ficha.php')">Citas</a></li>
							<!-- <li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('Create_ficha_interface.php')">Ficha</a></li> -->
						<?php   
					}?>
					<!-- <li class="nav-item"><a class="nav-link" href="#">Services</a></li> -->
					<!-- BUTTON as Administrador -->
					<?php
					if (isset($_SESSION['nivel']) && $_SESSION['nivel'] == 'administrador') {?>
							<li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('readMedico.php')">Medicos</a></li>
						<?php
					}?>
					<?php
					if (isset($_SESSION['nivel']) && $_SESSION['nivel'] == 'medico') {?>
							<li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('Read_Citas_Medico.php')">Citas Agendadas</a></li>
						<?php
					}?>
					<?php
					if (isset($_SESSION['nivel']) && $_SESSION['nivel'] == 'administrador') {?>
							<li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('Read_Paciente_Admin.php')">Pacientes</a></li>
						<?php
					}?>
					<?php
					if (isset($_SESSION['nivel'])) {?>
							<li class="nav-item"><a class="nav-link" href="javascript:cerrarSession()">Cerrar sesión</a></li>
						<?php
					}?>
					<?php
					if (!isset($_SESSION['nivel'])) {?>
						<div class="btn-container ">

							<!-- Modal LOGIN--> 
						<div class="modal fade" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">Iniciar Sesión</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="javascript: loginFetch()" id="formLogin">
									<div class="mb-3">
										<label for="email" class="form-label">Correo</label>
										<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
									</div>
									<div class="mb-3">
										<label for="password" class="form-label">Contraseña</label>
										<input type="password" class="form-control" id="password" name="password">
									</div>
									<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										<button type="submit" class="btn btn-primary">Aceptar</button>
									</div>
								</form>
							</div>
							</div>
						</div>
						</div>
							<!-- bootones para el login y singup -->
							<button type="button" class="btn btn-main-2 btn-icon btn-round-full" data-toggle="modal" data-target="#modal_login">Iniciar Sesión</button>
							<button type="button" class="btn btn-main-2 btn-icon btn-round-full" data-toggle="modal" data-target="#modal_singUp">Registrarse</button>

							<!-- Modal SingUP -->
							<div class="modal fade" id="modal_singUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLongTitle">Resgistrarse</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body" id="signup">
									<form action="javascript: SingUpFetch()" id="formSingUp">
										<div class="mb-3">
											<label for="email" class="form-label">Correo</label>
											<input type="email" class="form-control" id="email_sign_up" name="email" aria-describedby="emailHelp">
										</div>
										<div class="mb-3">
											<label for="password" class="form-label">Contraseña</label>
											<input type="password" class="form-control" id="password_sing_up" name="password">
										</div>
										<div class="mb-3">
											<label for="nombre" class="form-label">Nombre</label>
											<input type="text" class="form-control" id="nombre" name="nombre">
										</div>
										<div class="mb-3">
											<label for="apellido" class="form-label">Apellido</label>
											<input type="text" class="form-control" id="apellifo" name="apellido">
										</div>
										<div class="mb-3">
											<label for="genero" class="form-label" name="genero">Genero</label>
											<select class="form-control" id="genero" name="genero">
												<option value = "Masculino">Masculino</option>
												<option value = "Femenino">Femenino</option>
												<option value = "Otro">Otro</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="telefono" class="form-label">Telefono</label>
											<input type="number" class="form-control" id="telefono" name="telefono">
										</div>
										<div class="mb-3">
											<label for="direccion" class="form-label">Domicilio</label>
											<input type="text" class="form-control" id="direccion" name="direccion">
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											<button type="submit" class="btn btn-primary">Aceptar</button>
										</div>
									</form>
								</div>
								</div>
							</div>
							</div>
						</div>
					<?php
					}?>
				</ul>
			</div>
		</div>
	</nav>
</header>


<div class="container"id="contenido">

<!-- Slider Start -->
<section class="banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-12 col-xl-7">
				<div class="block">
					<div class="divider mb-3"></div>
					<span class="text-uppercase text-sm letter-spacing ">SOLUCIÓN TOTAL PARA EL CUIDADO DE LA SALUD</span>
					<h1 class="mb-3 mt-3">Su compañera de salud más confiable</h1>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section class="features">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="feature-block d-lg-flex">
					<div class="feature-item mb-5 mb-lg-0">
						<div class="feature-icon mb-4">
							<i class="icofont-support"></i>
						</div>
						<span>Casos de Emergencia</span>
						<h4 class="mb-3">1-800-700-6200</h4>
						<p>Obtenga apoyo en cualquier momento para emergencias. Hemos introducido el principio de la medicina familiar. Conéctese con nosotros para cualquier urgencia.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section about">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-4 col-sm-6">
				<div class="about-img">
					<img src="images/about/img-1.jpg" alt="" class="img-fluid">
					<img src="images/about/img-2.jpg" alt="" class="img-fluid mt-4">
				</div>
			</div>
			<div class="col-lg-4 col-sm-6">
				<div class="about-img mt-4 mt-lg-0">
					<img src="images/about/img-3.jpg" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-lg-4">
				<div class="about-content pl-4 mt-4 mt-lg-0">
					<h2 class="title-color">Cuidado Personal <br>& Vida Saludable</h2>

				</div>
			</div>
		</div>
	</div>
</section>
<section class="cta-section ">
	<div class="container">
		<div class="cta position-relative">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="counter-stat">
						<i class="icofont-doctor"></i>
						<span class="h3 counter" data-count="58">0</span>k
						<p>Personas Felices</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="counter-stat">
						<i class="icofont-flag"></i>
						<span class="h3 counter" data-count="700">0</span>+
						<p>Cirugías completadas</p>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="counter-stat">
						<i class="icofont-badge"></i>
						<span class="h3 counter" data-count="40">0</span>+
						<p>Doctores Expertos</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="counter-stat">
						<i class="icofont-globe"></i>
						<span class="h3 counter" data-count="20">0</span>
						<p>Sucursales en todo el mundo</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<!-- footer Start -->
<footer class="footer section gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 mr-auto col-sm-6">
				<div class="widget mb-5 mb-lg-0">
					<div class="logo mb-4">
						<img src="images/logo.png" alt="" class="img-fluid">
					</div>
					<p>Siguenos en nuestras redes sociales:</p>

					<ul class="list-inline footer-socials mt-4">
						<li class="list-inline-item">
							<a href="https://www.facebook.com/themefisher"><i class="icofont-facebook"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="https://twitter.com/themefisher"><i class="icofont-twitter"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="https://www.pinterest.com/themefisher/"><i class="icofont-linkedin"></i></a>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-lg-2 col-md-6 col-sm-6">
				<div class="widget mb-5 mb-lg-0">
					<h4 class="text-capitalize mb-3">Departmento</h4>
					<div class="divider mb-4"></div>

					<ul class="list-unstyled footer-menu lh-35">
						<li><a href="#!">Cirugía </a></li>
						<li><a href="#!">Cuidado de la mujer</a></li>
						<li><a href="#!">Radiología</a></li>
						<li><a href="#!">Cardiología</a></li>
						<li><a href="#!">Medicina</a></li>
					</ul>
				</div>
			</div>

			<div class="col-lg-2 col-md-6 col-sm-6">
				<div class="widget mb-5 mb-lg-0">
					<h4 class="text-capitalize mb-3">Soporte</h4>
					<div class="divider mb-4"></div>

					<ul class="list-unstyled footer-menu lh-35">
						<li><a href="#!">Términos y condiciones</a></li>
						<li><a href="#!">Política de privacidad</a></li>
						<li><a href="#!">Soporte de la empresa </a></li>
						<li><a href="#!">Preguntas frecuentes (FAQ)</a></li>
						<li><a href="#!">Licencia de la empresa</a></li>
					</ul>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="widget widget-contact mb-5 mb-lg-0">
					<h4 class="text-capitalize mb-3">Contáctanos</h4>
					<div class="divider mb-4"></div>

					<div class="footer-contact-block mb-4">
						<div class="icon d-flex align-items-center">
							<i class="icofont-email mr-3"></i>
							<span class="h6 mb-0">Soporte Disponible 24/7</span>
						</div>
						<h4 class="mt-2"><a href="mailto:support@email.com">soporte@email.com</a></h4>
					</div>

					<div class="footer-contact-block">
						<div class="icon d-flex align-items-center">
							<i class="icofont-support mr-3"></i>
							<span class="h6 mb-0">Lunes a Viernes : 08:30 - 18:00</span>
						</div>
						<h4 class="mt-2"><a href="tel:+23-345-67890">+23-456-6588</a></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

    <!-- 
    Essential Scripts
    =====================================-->
    <script src="plugins/jquery/jquery.js"></script>
    <script src="plugins/bootstrap/bootstrap.min.js"></script>
    <script src="plugins/slick-carousel/slick/slick.min.js"></script>
    <script src="plugins/shuffle/shuffle.min.js"></script>

    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA"></script>
    <script src="plugins/google-map/gmap.js"></script>
    
    <script src="js/script.js"></script>

  </body>
  </html>