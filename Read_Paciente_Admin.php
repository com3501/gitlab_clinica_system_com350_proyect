<DOCTYPE Html>
    <html> 
    <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/Read_Paciente_Admin.css">
    </head>
    <body>
        <div class="table-container">
            <h1 class="heading">Pacientes</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Genero</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Historia</th>
                    </tr>
                </thead>
                <tbody>
                <?php
    				include "conexion.php";
                    $sql2 = "SELECT id_paciente FROM historia";
					$sql = "SELECT id, nombre , apellido, genero, direccion, telefono, email From paciente";
					$resultado = $con->query($sql);
                    $resultado2 = $con->query($sql2);
                    $list = array();
                    $list =array();
                    while($datos2 = $resultado2 -> fetch_assoc()) {
                        $list[] = $datos2;
                        // print_r($datos2);
                    }
                    
                    if ($resultado !== false) {
                        while($datos = $resultado -> fetch_assoc()) { ?>
                            <tr>
                                <td><?= $datos['nombre']?></td>
                                <td><?= $datos['apellido'] ?></td>
                                <td><?= $datos['genero'] ?></td>
                                <td><?= $datos['direccion'] ?></td>
                                <td><?= $datos['telefono'] ?></td>
                                <td><?= $datos['email']?></td>
                                
						<td>
                                <?php
                            $id_p = $datos['id'];
                            $encontrado = false; 

                            foreach ($list as $datos2) {
                                if ($id_p == $datos2['id_paciente']) {
                                    $encontrado = true; 
                                    break; 
                                }
                            }

                            if ($encontrado) { ?>
                                <a href="javascript: MostrarHistoria('<?php echo $id_p?>')" class="segundo"> Mostrar</a>
                            <?php
                            } else { ?>
                                <a href="javascript: formHistoria('<?php echo $id_p?>')" class="primero" >Registrar</a>
                            <?php
                            }
                            ?>
                        </td> 
					</tr>
				<?php }
                }    
				?>
                </tbody>
            </table>
        </div>
    </body>

    </html>
