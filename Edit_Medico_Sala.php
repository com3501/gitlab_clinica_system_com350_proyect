<?php
include('conexion.php');
session_start();

$id_medico = $_GET['id'];


$sql = " SELECT m.nombre , m.apellido , m.edad, m.telefono , m.informacion , m.especialidad, s.id as id_sala_medico, s.num_consultorio as consultorio from medico m INNER JOIN sala s ON m.id_sala = s.id where m.id= $id_medico";
$sql2 = "SELECT id ,num_consultorio FROM sala WHERE estado ='Disponible'";

$resultado = $con->query($sql);
$resultado2 = $con->query($sql2);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Edicion Medico</title>
    <link rel="stylesheet" href="estilo.css">

    <style>
        .edicion-form {
            display: grid;
            grid-template-columns: 1fr 1fr;
        }
        .edicion-form .bloque{
            grid-column: 1 / 3;
        }
        .edicion{
            box-shadow: 0 0 20px 0 rgb(40, 62, 97, .3);
        }
        .edicion > * {
            padding: 1em;
            margin: 0 ;
        }
        .edicion-form p{
            margin: 0 ;
            padding: 1em;
        }
        .boton{
            background: #283E61;
            border: 0;
            padding: 10px 20px;
            color: #fff;
            text-align: center;
            margin: 0 auto;
            width: 20%;
            cursor: pointer;
            border-radius: 25px;
        }
        .container {
            max-width: 1100px;
            margin-left: auto;
            margin-right: auto;
            padding: 1.5em;
            color: #283E61;
        }
        .title {
            text-align: center;
            padding: 5;
            font-size: 3em;
            color: #283E61;
        }

        .title span {
            color: #CF3454;
        }
        .form-select{
            background: #CF3454;
            border: none;
            padding: .50em;
            width: 50%;
            height: 50%;
            margin: 0 auto;
            color: #fff;
            text-align: center;
            align-self: flex-start;
        }
        #contenido {
            margin-top: 10px; 
            margin-bottom: 10px; 
            display: flex;
            justify-content: center;
        }
        .form-group {
            padding: auto;
            float: left;
            margin-bottom: 5px;
            font-weight: bold;
            display: flex;
            flex-direction: column;
        }
        .form-textarea{
        width: 850px;
        height: 150px; 
        resize: vertical;
        text-align: left;
        margin: 0 auto;
}
        
    </style>
   
</head>
<body>
<div class="container">
    <?php
        if ($resultado->num_rows > 0) {
            $data = $resultado->fetch_assoc();
            ?>
            <?php
            $id_sala_antiguo = $data['id_sala_medico'];
            ?>
            <h1 class="title">Editar  <span> Medico</h1>  
            <form method="POST" action="javascript: createEdit(<?php echo $id_medico; ?>,<?php echo $id_sala_antiguo; ?>)" id="formMedico"> 
                <div class = "edicion">   
                    <div class="edicion-form">
                            <p>
                                <label class="form-label">NOMBRE:</label><br>
                                <input type="text" class="form-field" name="nombre" value="<?php echo $data['nombre'] ?>" >
                            </p>
                            <p>
                                <label class="form-label">APELLIDO:</label><br>
                                <input type="text" class="form-field" name="apellido" value="<?php echo $data['apellido'] ?>" >
                            </p>
                            <p>
                                <label class="form-label">EDAD:</label><br>
                                <input type="text" class="form-field" name="edad" value="<?php echo $data['edad'] ?>" >
                            </p>
                            <p>
                                <label class="form-label">TELEFONO:</label><br>
                                <input type="text" class="form-field" name="telefono" value="<?php echo $data['telefono'] ?>">
                            </p>
                            
                            <p>
                                <label class="form-label">ESPECIALIDAD:</label><br>
                                <input type="text" class="form-field" name="especialidad" value="<?php echo $data['especialidad'] ?>" >
                            </p>
                            <p class="form-group">
                                <label for="sala" class="form-label" required>CONSULTORIO:</label>
                                <select class="form-select" id_sala="id" name="sala_nuevo" required>
                                <option value="<?php echo $id_sala_antiguo?>" selected><?php echo $data['consultorio'];?></option>
                                <?php while ($sala = $resultado2->fetch_assoc()){
                                ?>
                                <option value="<?php echo $sala['id']?>"><?php echo $sala['num_consultorio'];?> </option>
                                <?php
                                }?>
                                </select>
                            </p>
                            
                        </div>
                        <div class ="edicion-form">
                            <p>
                                <label class="form-label">INFORMACION:</label><br>
                                <!--<input type="text" class="form-field" name="informacion" value="<?php echo $data['informacion'] ?>" >-->
                                <textarea class="form-textarea" name="informacion" style="width: 800px;" rows="4"><?php echo $data['informacion'] ?></textarea>
                            </p>
                            </div>
                </div>
            <div class="boton-container">
                <!-- <input  value="Enviar"> -->
                <button type="submit" class="boton">GUARDAR</button></a>
            </div>
        <?php
        }
        ?> 
        </form>
        
    </div>
</body>
</html>



