

<?php
include('conexion.php');
$sql = "SELECT m.id as 'id_medico', m.nombre as 'nombre', m.apellido as 'apellido' , m.edad as 'edad', m.telefono as 'telefono',m.email as 'email', 
m.especialidad as 'especialidad', m.informacion as 'informacion', s.id as 'id_sala', s.num_consultorio as 'sala' from sala s inner join medico m on s.id = m.id_sala";
$resultado = $con->query($sql);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Título de tu página</title>
        <link rel="stylesheet" type="text/css" href="css/readMedico.css">    
    </head>
    <body>
        <div class="table-container">
            <h1 class ="heading"> MEDICOS</h1>
            <?php
                if ($resultado->num_rows > 0) {
            ?>
                <table class ="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Edad</th>
                        <th>Teléfono</th>
                        <th>Email</th>
                        <th>Especialidad</th>
                        <th>Información</th>
                        <th>Consultorio</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                     <?php while ($row = $resultado->fetch_assoc()) { ?>
                    <tr>
                        <td><?php echo $row['nombre'] ?></td>
                        <td><?php echo $row['apellido'] ?></td>
                        <td><?php echo $row['edad'] ?></td>
                        <td><?php echo $row['telefono'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $row['especialidad'] ?></td>
                        <td><?php echo $row['informacion'] ?></td>
                        <td><?php echo $row['sala']?></td>
                        <td>
                        <a href="javascript: registrarEdit('<?php echo $row['id_medico']?>')" class="primero"> Editar</a>      
                        </td> 
                    </tr>
                    <?php } ?>
                </table>
                
            <div class="boton">
            <a href="javascript: cargarContenido('registro_m.php')" style = "margin: 5px;">
            <button class ="segundo">Añadir Nuevo Médico</button>
            </div>

          
        <!-- <button type="button" action="registro_m.html"></button> -->
    <?php
    } else {
        echo "La tabla no tiene datos que mostrar";
    }

    $con->close();
    ?>
    </div>
</body>
</html>
