-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2023 at 12:07 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinica`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `nivel` varchar(60) DEFAULT 'Administrador',
  `email` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `administrador`
--

INSERT INTO `administrador` (`id`, `password`, `nombre`, `nivel`, `email`) VALUES
(1, '6060', 'Jose', 'Administrador', 'jgms@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `paciente_id` int(11) DEFAULT NULL,
  `medico_id` int(11) DEFAULT NULL,
  `fechaCita` time DEFAULT NULL,
  `estado` enum('Disponible','No Disponible') DEFAULT NULL,
  `fecha_mes` date DEFAULT curdate()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `citas`
--

INSERT INTO `citas` (`id`, `paciente_id`, `medico_id`, `fechaCita`, `estado`, `fecha_mes`) VALUES
(4, 52, 1, '09:00:00', NULL, '2023-09-03'),
(6, 51, 1, '08:00:00', NULL, '2023-09-03'),
(20, 50, 2, '13:00:00', NULL, '2023-09-03'),
(29, 50, 4, '16:00:00', NULL, '2023-09-04'),
(30, 50, 1, '08:00:00', NULL, '2023-09-05');

-- --------------------------------------------------------

--
-- Table structure for table `descripcion`
--

CREATE TABLE `descripcion` (
  `id` int(11) NOT NULL,
  `sintomas` varchar(300) DEFAULT NULL,
  `diagnostico` varchar(300) DEFAULT NULL,
  `tratamiento` varchar(300) DEFAULT NULL,
  `receta` varchar(300) DEFAULT NULL,
  `fecha` date DEFAULT curdate(),
  `id_historia` int(11) DEFAULT NULL,
  `id_medico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `descripcion`
--

INSERT INTO `descripcion` (`id`, `sintomas`, `diagnostico`, `tratamiento`, `receta`, `fecha`, `id_historia`, `id_medico`) VALUES
(1, 'Temperatura corporal superior a 38?C.', 'Gripe', 'Antibi?ticos para la Gripe.', 'Amoxicilina 500mg cada 8 horas durante 5 d?as', '2023-09-02', 1, 1),
(2, 'Dolor persistente en la cabeza.', 'Hipertensi?n', 'Control de la Hipertensi?n', 'Losart?n 50mg una vez al d?a Tomar por la ma?ana', '2023-09-05', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `historia`
--

CREATE TABLE `historia` (
  `id` int(11) NOT NULL,
  `altura` varchar(60) DEFAULT NULL,
  `peso` varchar(60) DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `num_emergencia` varchar(60) DEFAULT NULL,
  `tipo_sanngre` varchar(60) DEFAULT NULL,
  `id_paciente` int(11) NOT NULL,
  `alergia` varchar(200) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `historia`
--

INSERT INTO `historia` (`id`, `altura`, `peso`, `direccion`, `num_emergencia`, `tipo_sanngre`, `id_paciente`, `alergia`, `fecha_nac`) VALUES
(1, '1.80', '78', 'Calle Mancesped 22', '70707070', 'O+', 50, 'nueces', '1999-06-25'),
(2, '1.80', '80', 'Calle Loa 25', '72707070', 'A', 51, 'Mani', '1998-10-03'),
(3, '1.55', '50', 'Av. Americas 78', '73707070', 'AB', 52, 'Camaron', '2002-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `medico`
--

CREATE TABLE `medico` (
  `id` int(11) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `apellido` varchar(60) DEFAULT NULL,
  `edad` varchar(60) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `informacion` varchar(200) DEFAULT NULL,
  `especialidad` varchar(20) DEFAULT NULL,
  `nivel` varchar(60) DEFAULT 'Medico',
  `email` varchar(20) DEFAULT NULL,
  `id_sala` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `medico`
--

INSERT INTO `medico` (`id`, `password`, `nombre`, `apellido`, `edad`, `telefono`, `informacion`, `especialidad`, `nivel`, `email`, `id_sala`) VALUES
(1, '8080', 'Carlos', 'Garcia', '38', 65656565, 'Estudios Terminados en 2012', 'Medico General', 'Medico', 'carlosgar@gmail.com', 1),
(2, '6060', 'Pablo', 'Vargas', '50', 60606060, 'Estudios Terminados en 2005', 'Medico General', 'Medico', 'pvar@gmail.com', 2),
(3, '6565', 'Alejandra', 'Gonzales', '35', 62606060, 'Estudios Terminados en 2013', 'Medico General', 'Medico', 'alegon@gmail.com', 3),
(4, '6363', 'Pedro', 'Rojas', '35', 78999999, 'Estudios Terminados en 2015 en la Universidad San Francisco Xavier', 'Medico General', 'Medico', 'ikikik@gmail.com', 5),
(7, '7358', 'Alejandra', 'Velasquez', '27', 71727172, 'Termino sus Estudios en 2018', 'Medico Oftalmologo', 'Medico', 'zebu@mailinator.com', 9),
(8, '1234', 'Pedro', 'Paredes', '39', 78787878, 'Termino sus estudios en el 2010\r\nRealizo su especialidad en el hospital Santa Barbara', 'Medico Dermatologo', 'Medico', 'pedroparedes@gmail.c', 6),
(9, '5678', 'Carolina', 'Quispe', '40', 76747674, 'Termino sus estudio es la Universidad San Francisco Xavier y realiza su especialidad en el Cochabamba', 'Medico Cardiologo', 'Medico', 'carolinaQ@gmail.com', 8); --------------------------------------------------------

--
-- Table structure for table `paciente`
--

CREATE TABLE `paciente` (
  `id` int(11) NOT NULL,
  `password` varchar(12) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apellido` varchar(30) DEFAULT NULL,
  `genero` enum('Masculino','Femenino','Otro') DEFAULT NULL,
  `direccion` varchar(70) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `nivel` varchar(60) DEFAULT 'Paciente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paciente`
--

INSERT INTO `paciente` (`id`, `password`, `nombre`, `apellido`, `genero`, `direccion`, `telefono`, `email`, `nivel`) VALUES
(50, '7070', 'Felipe', 'Perez', 'Masculino', 'Calle Mancesped 22', 70707070, 'fperez@gmail.com', 'Paciente'),
(51, '5151', 'Juan', 'Dias', 'Masculino', 'Calle Loa 25', 72707070, 'juand@gmail.com', 'Paciente'),
(52, '8800', 'Valeria', 'Ortega', 'Femenino', 'Av. Americas 78', 73707070, 'valeortega@gmail.com', 'Paciente'),
(53, '8800', 'Alejandra', 'Ortega', 'Femenino', 'Av. Americas 78', 73707070, 'valeortega@gmail.com', 'Paciente'),
(54, '7676', 'Estefani', 'Coca', 'Femenino', 'Calle Tarija #158', 65627070, 'estefaniCom@gmail.com', 3, 1, 'Paciente'),
(55, '9090', 'Marta', 'Rivera', 'Femenino', 'Calle Urcullo #312', 65628070, 'martar@gmail.com', 2, 1, 'Paciente'),
(56, '7171', 'Rosa', 'Flores', 'Femenino', 'Calle La Paz #312', 65909070, 'rosaFlo@gmail.com', 2, 1, 'Paciente'),
(57, '2121', 'Jorge', 'Lopez', 'Masculino', 'Av. Jaime Mendoza #333', 65323212, 'jorgeLo@gmail.com', 4, 1, 'Paciente'),
(58, '3232', 'Raul', 'Torrez', 'Masculino', 'Av. Del Maestro #100', 60252525, 'raultorrez@gmail.com', 3, 1, 'Paciente');
-- --------------------------------------------------------

--
-- Table structure for table `sala`
--

CREATE TABLE `sala` (
  `id` int(11) NOT NULL,
  `num_consultorio` varchar(30) DEFAULT NULL,
  `estado` enum('Disponible','Ocupado') DEFAULT 'Disponible'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sala`
--

INSERT INTO `sala` (`id`, `num_consultorio`, `estado`) VALUES
(1, 'C-001', 'Ocupado'),
(2, 'C-002', 'Ocupado'),
(3, 'C-003', 'Ocupado'),
(4, 'C-004', 'Disponible'),
(5, 'C-005', 'Ocupado'),
(6, 'C-006', 'Ocupado'),
(8, 'D-001', 'Ocupado'),
(9, 'D-002', 'Ocupado'),
(10, 'D-003', 'Disponible'),
(11, 'D-004', 'Disponible'),
(12, 'D-005', 'Disponible'),
(13, 'D-006', 'Disponible');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medico_id` (`medico_id`),
  ADD KEY `paciente_id` (`paciente_id`);

--
-- Indexes for table `descripcion`
--
ALTER TABLE `descripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_historia` (`id_historia`),
  ADD KEY `id_medico` (`id_medico`);

--
-- Indexes for table `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_paciente` (`id_paciente`);

--
-- Indexes for table `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_medico_sala` (`id_sala`);

--
-- Indexes for table `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `descripcion`
--
ALTER TABLE `descripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `historia`
--
ALTER TABLE `historia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `medico`
--
ALTER TABLE `medico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `sala`
--
ALTER TABLE `sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`medico_id`) REFERENCES `medico` (`id`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`);

--
-- Constraints for table `descripcion`
--
ALTER TABLE `descripcion`
  ADD CONSTRAINT `descripcion_ibfk_1` FOREIGN KEY (`id_historia`) REFERENCES `historia` (`id`),
  ADD CONSTRAINT `descripcion_ibfk_2` FOREIGN KEY (`id_medico`) REFERENCES `medico` (`id`);

--
-- Constraints for table `historia`
--
ALTER TABLE `historia`
  ADD CONSTRAINT `historia_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id`);

--
-- Constraints for table `medico`
--
ALTER TABLE `medico`
  ADD CONSTRAINT `FK_medico_sala` FOREIGN KEY (`id_sala`) REFERENCES `sala` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
