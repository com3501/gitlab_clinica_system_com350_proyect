<?php
include('conexion.php');

$sql="SELECT id, num_consultorio, estado FROM sala WHERE estado = 'Disponible'";

$resultado = $con->query($sql);

?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="estilo.css">
<style>

  #contenido {
    margin-top: 10px; 
    margin-bottom: 10px; 
    display: flex;
    justify-content: center;
  }
  
  .conteiner1-1 {
    background-color: #ffffff;
    padding: 20px;
    border-radius: 5px;
    box-shadow: 0 0 20px 0 rgb(40, 62, 97, .3);
    width: 400px;
  }
  
  /*.conteiner1-1 h2 {
    margin-bottom: 20px;
    text-align: center;
  }*/
  
  .form-group {
    margin-bottom: 15px;
  }
  
  .form-group label {
    display: block;
    margin-bottom: 5px;
    font-weight: bold;
  }
  
  .form-group input {
    width: 100%;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
  }
  
  .b {
    background-color: #283E61;
    color: #ffffff;
    padding: 10px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    border-radius: 25px;
  }
  .contenedorT {
  margin-bottom: 20px; /* Agrega espacio entre los contenedores */
  }
  .title {
    text-align: center;
    padding: 5;
    font-size: 3em;
    color: #283E61;
}

.title span {
    color: #CF3454;
}
.form-group-consultorio{
  float: left;
  display: block;
    margin-bottom: 5px;
    font-weight: bold;
    display: flex;
    flex-direction: column;
}

.form-select{
    background: #CF3454;
    border: none;
    padding: .50em;
    width: 175%;
    height: 35%;
    margin: 0 auto;
    color: #fff;
    text-align: center;
    align-self: flex-start;
    
}
 </style>
<title>Formulario de Registro</title>
  
</head>
<body>
  <form method="POST" action="javascript: registrarMedico()" id="formMedico">
  <h1 class="title">Registro <span> Medico</span></h1>
    <div class="row">
      <div class="column">
          <div class="form-group">
            <label for="nombre">NOMBRE:</label>
            <input type="text" id="nombre" name="nombre" required>
          </div>
          <div class="form-group">
            <label for="apellido">APELLIDO:</label>
            <input type="text" id="apellido" name="apellido" required>
          </div>
          <div class="form-group">
            <label for="fechaNacimiento">EDAD:</label>
            <input type="text" id="fechaNacimiento" name="edad" required>
          </div><br>
      </div>

      <div class="column">
        <div class="form-group">
        <label for="telefono">TELÉFONO:</label>
        <input type="text" id="telefono" name="telefono" required>
      </div>
        <div class="form-group">
          <label for="especialidad">ESPECIALIDAD:</label>
          <input type="text" id="especialidad" name="especialidad" required>
        </div>
        <div class="form-group">
          <label for="email">EMAIL:</label>
          <input type="email" id="email" name="email" required>
        </div>
        <div class="form-group-consultorio">
          <label for="consultorio">CONSULTORIO:</label>
          <select class="form-select" id="id_sala" name="sala" required>
          <?php
          if($resultado){
            while ($sala = $resultado->fetch_assoc()){
            ?>
              <option value="<?php echo $sala['id']?>"><?php echo $sala['num_consultorio'];?> </option>
          <?php  
            }
          }
          ?>
          </select>
          <!-- <input type="text" id="consultorio" name="consultorio" required> -->
        </div>
      </div>
    </div>
    <div class= "row">
      <div class="column">
        <div class="form-group">
          <label for="informacion">Información:</label>
          <textarea id="informacion" name="informacion" style="width: 800px;" rows="4" required></textarea>
          <button type="submit" class="b">Registrar</button>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
