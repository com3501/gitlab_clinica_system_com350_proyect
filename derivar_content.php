<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>


/* ANTERIRO */

        .table_derivar{
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
            color: white;

        }

        .thead_derivar{
            background-color: #283E61 !important;
        }

        .table_derivar, .th_derivar, .td_derivar {
        border: 1px solid #ccc;
        }

        .th_derivar, .td_derivar {
        padding: 10px;
        text-align: center;
        }

        .hora{
            background-color: #283E61;
            padding: 10px;
            text-align: center;
        }
        .boton_horarios {
        width: 100%;
        height: 40px;
        background-color: white;
        border: 1px none;
        cursor: pointer;
        border-radius: 15px; 
        
        }

        /* button.red {
        background-color: red;
        } */
    </style>
</head>
<body>
    <div class="card-body">
        <form  id="form_derivar">
            <div class="row mb-3">
                <div class="col-md-6 mb-3">
                    <div class="contenedor3">
                        <label for="dia" class="form-label" required>Día:</label>
                        <select class="form-select" id="dia_derivacion" name="dia_derivacion" required></select>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="contendor3">
                        <label for="hora_derivacion" class="form-label" required>Hora:</label>
                        <select class="form-select" id="hora_derivacion" name="hora_derivacion" required></select>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-6 mb-3">
                    <div class="contenedor3">
                        <label for="especialidad" class="form-label" required>Especialidad: </label>
                        <select class="form-select" id="especialidad" name="especialidad" required></select>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="contenedor3">
                        <label for="medico" class="form-label" required>Médico: </label>
                        <select class="form-select" id="medico" name="id_medico" required>
                            <option selected>Selecciona una especialidad</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="contenedor3">
                        <button type="submit" class="btn btn-primary">Derivar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    <div class="contenedor2">
        <div class="feature-icon mb-4">
            <!-- <i class="icofont-ui-clock"></i> -->
        </div>
        <span> <h3 sytle="color: white !important;">Horario</h3></span>

        <div class="contenedor3" id="tabla_horarios">
        <!-- <div id="tabla_horarios">
        </div> -->
        </div>    
    </div>
</body>
</html>