function cargarContenido(abrir) {

    var contenedor;
    contenedor = document.getElementById('contenido');
    fetch(abrir)
        .then(response => response.text() )
        .then(data => contenedor.innerHTML = data);
}


function cargarContenidoInicio() {

  fetch('inde.php')
      .then(response => response.text() )
      .then(data => window.location.href = 'inde.php');
}

function derivar(id) {
  
  var contenedor;
  contenedor = document.getElementById('derivar');

  fetch('derivar_content.php')
      .then(response => response.text())
      .then(data => {
          contenedor.innerHTML = data;
          form = document.getElementById('form_derivar');
          form.setAttribute("action", "javascript:registrar_derivacion("+ id + ")");
          //  ESPECIALIDADES
          // Obtén referencias al select de especialidades
          const especialidadSelect = document.getElementById('especialidad');

          fetch('Espe_Derivar.php')
          .then(response => response.json())
          .then(data => {
            // var datos = JSON.parse(data);
            // Agrega las opciones de especialidades al select de especialidades

            data.forEach(especialidad => {
              const option = document.createElement('option');
              option.value = especialidad;
              option.textContent = especialidad;
              especialidadSelect.appendChild(option);
            });
          })
          .catch(error => {
            console.error('Error al cargar las especialidades:', error);
          })

          //  MÉDICOS
          // Obtén una referencia al select de médicos
            const medicoSelect = document.getElementById('medico');

            // Maneja el evento de cambio en el select de especialidades
            especialidadSelect.addEventListener('change', () => {
              const especialidadSeleccionada = especialidadSelect.value;
              console.log(especialidadSeleccionada)
              // Realiza una solicitud fetch para obtener los médicos de la especialidad seleccionada
              fetch('Espe_Medico_Derivar.php?especialidad='+especialidadSeleccionada)
                // console.log(especialidadSeleccionada)
                .then(response => response.json())
                .then(data => {
                  // Limpia el select de médicos
                  medicoSelect.innerHTML = '';
                  // Agrega las opciones de médicos al select de médicos
                  data.forEach(medico => {
                    console.log(medico.nombre + " " + medico.apellido);
                    const option = document.createElement('option');
                    option.value = medico.id; // Puedes usar un identificador único del médico
                    option.textContent = medico.nombre + ' ' + medico.apellido ; // Nombre del médico
                    medicoSelect.appendChild(option);
                  });
                })
                // .catch(error => {
                //   console.error('Error al cargar los médicos:', error);
                // });
            });


          const hora = document.getElementById('hora_derivacion');
          const horas = ["08", "09","10", "11", "14", "15", "16", "17"]
          
      
          for (let i = 0; i <horas.length; i++) {
              const option = document.createElement("option");
      
              var horaFormateada = `${horas[i]}:${'00'}:${'00'}`;
      
              // if(horas[i] == "8" || horas[i] == "9"){
              //     horaFormateada = `${'0'}${horas[i]}:${'00'}:${'00'}`;
              // }

              option.value = horaFormateada;
              option.text =  horas[i] + ":00";
              hora.appendChild(option);
          }

          const selectDiaSemana = document.getElementById("dia_derivacion");
          
      
          const diasSemana = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"];
          const fechaActual = new Date();
      
          
      
          // Calcula la fecha del primer día de la semana (Domingo)
          const primerDiaSemana = new Date(fechaActual);
          // primerDiaSemana.setDate(fechaActual.getDate() - diaActual);
      
          // Genera las opciones del select
          for (let i = 0; i < diasSemana.length; i++) {
              const option = document.createElement("option");
      
              option.text = diasSemana[i];
              
              // Calcula la fecha correspondiente al día de la semana
              const fechaDiaSemana = new Date(primerDiaSemana);
              // 
              // 
              // IMPORTANTE ESTO SE CORRIGE EL -2, HAY PROBLEMAS CONFORME PASAN LOS DIAS
              // 
              // 
              fechaDiaSemana.setDate(primerDiaSemana.getDate()+i-2);
      
              // Formatea la fecha en formato "dd/mm/yyyy"
              const dd = String(fechaDiaSemana.getDate()).padStart(2, "0");
              const mm = String(fechaDiaSemana.getMonth() + 1).padStart(2, "0");
              const yyyy = fechaDiaSemana.getFullYear();
              const fechaFormateada = `${yyyy}-${mm}-${dd}`;
              console.log(fechaFormateada);
              option.value = fechaFormateada;
              selectDiaSemana.appendChild(option);
          }
          console.log("antes del fetch 2");
          
          var tabla = document.getElementById('tabla_horarios');
          fetch('datosTabla.php')
          .then(response => response.text())
          .then(data => {
          // console.log("antes JSON");
            var datos = JSON.parse(data);
            var result = dibujarTabla(datos);
            tabla.innerHTML = result});
          function dibujarTabla(datos){
            console.log("dentro de la función");
            var html = "";
            html +=`
            <table class="table_derivar">
              <thead class="thead_derivar">
                <tr>
                  <th class= "th_derivar"> HORA </th>
                  <th class= "th_derivar"> LUNES </th>
                  <th class= "th_derivar"> MARTES </th>
                  <th class= "th_derivar"> MIERCOLES </th>
                  <th class= "th_derivar"> JUEVES </th>
                  <th class= "th_derivar"> VIERNES </th>
                </tr>
              </thead>
              <tbody>`
            for (let i = 0; i < 8 ; i++) {
              html += `<tr>`;
              html += `<td class="hora"> ${horas[i]}:00 </td>`;
              for(let j = 0; j<5; j++){
                var indice = 0;
                console.log("al final");
                var rojo = false;
                while(indice < datos.length){
                  var fila = datos[indice];
                  const cadena = datos[indice].hora;
                  const parte = cadena.split(":");
                  const numero = parte[0];
                  const day = new Date(fila.dia);
                  if(numero == horas[i] && day.getDay() == j){
                    // html += `<td><button class="${rojo ? 'red' : ''}"></button></td>`;
                    html += `<td class= "td_derivar"><button style="background-color: #CF3454;" class="boton_horarios"></button></td>`;
                    rojo = true;
                    break;
                  }
                  indice++;
                }
                if(!rojo){
                  html += `<td class= "td_derivar"><button style="background-color: white;" class="boton_horarios"></button></td>`;
                }
              }
              html += `</tr>`;
            }
            html += `</tbody>
            </table>`;
            return html;
          }

          // const scriptElement = document.createElement('script');
          // scriptElement.innerHTML = data;
          // document.body.appendChild(scriptElement);
      });
}

function cargarSacarFicha() {
  
  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch('Create_ficha_interface.php')
  .then(response => response.text())
  .then(data => {
      contenedor.innerHTML = data;
    const hora = document.getElementById('hora');
    const horas = ["08", "09","10", "11", "14", "15", "16", "17"]
    

    for (let i = 0; i <horas.length; i++) {
        const option = document.createElement("option");

        var horaFormateada = `${horas[i]}:${'00'}:${'00'}`;

        option.value = horaFormateada;
        option.text =  horas[i] + ":00";
        console.log(option.text + "ultimo");
        hora.appendChild(option);
    }

    const selectDiaSemana = document.getElementById("dia");
    

    const diasSemana = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"];
    const fechaActual = new Date();

    

    // Calcula la fecha del primer día de la semana (Domingo)
    const primerDiaSemana = new Date(fechaActual);
    // primerDiaSemana.setDate(fechaActual.getDate() - diaActual);

    // Genera las opciones del select
    for (let i = 0; i < diasSemana.length; i++) {
        const option = document.createElement("option");

        option.text = diasSemana[i];
        
        // Calcula la fecha correspondiente al día de la semana
        const fechaDiaSemana = new Date(primerDiaSemana);
        // 
        // 
        // IMPORTANTE ESTO SE CORRIGE EL -2, HAY PROBLEMAS CONFORME PASAN LOS DIAS
        // 
        // 
        fechaDiaSemana.setDate(primerDiaSemana.getDate()+i-2);

        // Formatea la fecha en formato "dd/mm/yyyy"
        const dd = String(fechaDiaSemana.getDate()).padStart(2, "0");
        const mm = String(fechaDiaSemana.getMonth() + 1).padStart(2, "0");
        const yyyy = fechaDiaSemana.getFullYear();
        const fechaFormateada = `${yyyy}-${mm}-${dd}`;
        console.log(fechaFormateada);
        option.value = fechaFormateada;
        selectDiaSemana.appendChild(option);
    }
    console.log("antes del fetch 2");
    
    var tabla = document.getElementById('tabla_horarios');
    fetch('datosTabla.php')
    .then(response => response.text())
    .then(data => {
    // console.log("antes JSON");
      var datos = JSON.parse(data);
      var result = dibujarTabla(datos);
      tabla.innerHTML = result});
    function dibujarTabla(datos){
      console.log("dentro de la función");
      var html = "";
      html +=`
      <table>
        <thead>
          <tr>
            <th> HORA </th>
            <th> LUENES </th>
            <th> MARTES </th>
            <th> MIERCOLES </th>
            <th> JUEVES </th>
            <th> VIERNES </th>
          </tr>
        </thead>
        <tbody>`
      for (let i = 0; i < 8 ; i++) {
        html += `<tr>`;
        html += `<td class="hora"> ${horas[i]}:00 </td>`;
        for(let j = 0; j<5; j++){
          var indice = 0;
          console.log("al final");
          var rojo = false;
          while(indice < datos.length){
            var fila = datos[indice];
            const cadena = datos[indice].hora;
            const parte = cadena.split(":");
            const numero = parte[0];
            const day = new Date(fila.dia);
            if(numero == horas[i] && day.getDay() == j){
              // html += `<td><button class="${rojo ? 'red' : ''}"></button></td>`;
              html += `<td><button style="background-color: #CF3454;"></button></td>`;
              rojo = true;
              break;
            }
            indice++;
          }
          if(!rojo){
            html += `<td><button style="background-color: white;"></button></td>`;
          }
        }
        html += `</tr>`;
      }
      html += `</tbody>
      </table>`;
      return html;
    }



          // const scriptElement = document.createElement('script');
          // scriptElement.innerHTML = data;
          // document.body.appendChild(scriptElement);
      });
}

function formHistoria(id) {

  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch('listar.php?id='+id)
      .then(response => response.text())
      .then(data => { contenedor.innerHTML = data; });
}


function MostrarHistoria(id) {
  console.log(id);
  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch('Mostrar_Historia.php?id='+id)
      .then(response => response.text())
      .then(data => { contenedor.innerHTML = data; });
}

function formDescripcion(id) {
  console.log(id);
  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch('historia_medico.php?id='+id)
      .then(response => response.text())
      .then(data => { contenedor.innerHTML = data; });
}

function Registrar_historia_for_medico(id) {
  console.log(id);
  var contenedor;
  contenedor = document.getElementById('contenido');
  var formulario = document.getElementById("form_descripcion");
  var parametros = new FormData(formulario);
  fetch('create_historia_medico.php?id='+id, {
    method: "POST",
    body: parametros
  })
      .then(response => response.json())
      .then(data => { 
        if (data.redirect) {
          // console.log(data.redirect);
          fetch("Mostrar_Historia.php?id="+data.redirect)
          .then(response => response.text())
          .then(data => contenedor.innerHTML = data);
        }
      });
      
}

function registrar_derivacion(id) {
  console.log(id);
  var contenedor;
  contenedor = document.getElementById('derivar');
  var formulario = document.getElementById("form_derivar");
  var parametros = new FormData(formulario);
  fetch('createFicha.php?id='+id, {
    method: "POST",
    body: parametros
  })
      .then(response => response.json())
      .then(data => {   
        console.log(data.mensaje);
        if (data.mensaje) {
          
          contenedor.innerHTML = data.mensaje;
        }
      });
      
}


function Historia_for_paciente(fecha, id) {
  console.log(id);
  console.log(fecha + "fecha");
  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch('Mostrar_Historia.php?id='+id+'&fecha='+fecha)
      .then(response => response.text())
      .then(data => { contenedor.innerHTML = data; });
}

function registrarHistoria() {
  // console.log("entro");
  var contenedor;
  contenedor = document.getElementById('contenido');
  var formulario = document.getElementById("formHistoria");
  var parametros = new FormData(formulario);
  fetch("insertar.php",
      {
          method: "POST",
          body: parametros
      })
      .then(response => response.json())
      .then(data => {
        if (data.redirect) {
          fetch("Mostrar_Historia.php?id="+data.id)
          .then(response => response.text())
          .then(data => contenedor.innerHTML = data);
        }
      });
}


function cerrarSession() {
  // console.log("entro");
  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch("cerrarsession.php")
      .then(response => response.json())
      .then(data => {
        if (data.redirect) {
          window.location.href = data.redirect;
        }
      });
}

function registrarFicha() {
  
  // console.log("entro");
  var contenedor;
  contenedor = document.getElementById('contenido');
  var formulario = document.getElementById("formFicha");
  var parametros = new FormData(formulario);
  fetch("createFicha.php",
  {
    method: "POST",
    body: parametros
  })
  .then(response => response.json())
  .then(data => {
    if (data.redirect) {
      fetch("ms_ficha.html")
      .then(response => response.text())
      .then(data => contenedor.innerHTML = data);
    }
  });

  
}

function registrarMedico() {
  // console.log("entro");
  console.log("entrodepuesd del post");
  var contenedor;
  contenedor = document.getElementById('contenido');
  var formulario = document.getElementById("formMedico");
  var parametros = new FormData(formulario);
  fetch("createMedico.php",
      {
          method: "POST",
          body: parametros
      })
      .then(response => response.json())
      .then(data => {
        if (data.redirect) {
          // MENSAJE CON EXITO
          fetch("ms_reg_Medico.html")
          .then(response => response.text())
          .then(data => contenedor.innerHTML = data);
        }
      });
}
function registrarEdit(id) {
  console.log(id);
  var contenedor;
  contenedor = document.getElementById('contenido');
  fetch('Edit_Medico_Sala.php?id='+id)
      .then(response => response.text())
      .then(data => { contenedor.innerHTML = data; });
}

function createEdit(id_medico, id_sala_antiguo) {
  console.log(id_sala_antiguo + "sala");
  // console.log(id_medico);
  var contenedor;
  contenedor = document.getElementById('contenido');
  var formulario = document.getElementById("formMedico");
  var parametros = new FormData(formulario);
  fetch('create_Edit_sala.php?id_sala_antiguo='+ id_sala_antiguo +'&id_medico='+id_medico,
  {
    method: "POST",
    body: parametros
  })
      .then(response => response.json())
      .then(data => { 
        if(data.redirect){
          console.log(data.redirect + "redirectionando");
          // MENSAJE CON EXITO DE ACTUALIZACIÓN
          fetch("ms_Act_Medico.html")
          .then(response => response.text())
          .then(data => contenedor.innerHTML = data);
        }
      });
}

function loginFetch() {
  
  var contenedor;
  contenedor = document.getElementById('contenido');
  var formulario = document.getElementById("formLogin");
  var parametros = new FormData(formulario);
  fetch("autenticar.php",
      {
          method: "POST",
          body: parametros
      })
      .then(response => response.json())
      .then(data => {
        if (data.redirect) {
          window.location.href = data.redirect;
        }
      });
}
function SingUpFetch() {
  
  var contenedor;
  contenedor = document.getElementById('signup');
  var formulario = document.getElementById("formSingUp");
  var parametros = new FormData(formulario);
  fetch("Registrar.php",
      {
          method: "POST",
          body: parametros
      })
      .then(response => response.json())
      .then(data => {
        if (data.mensaje) {
          contenedor.innerHTML = data.mensaje;
        }
      });
}
